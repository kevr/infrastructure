- name: Install required packages
  pacman:
    state: present
    name:
      - asciidoc
      - highlight
      - make
      - sudo
      - uwsgi-plugin-cgi
      - python-poetry
      - gcc
      - pkg-config

- name: Install the cgit package
  pacman:
    state: present
    name:
      - cgit-aurweb
  register: cgit

- name: Install the git package
  pacman:
    state: present
    name:
      - git
  register: git

- name: Make aur user
  user: name="{{ aurweb_user }}" shell=/bin/bash createhome=yes
  register: aur_user

- name: Create .ssh for the aur user
  file: path={{ aur_user.home }}/.ssh state=directory owner={{ aur_user.name }} group={{ aur_user.name }} mode=0700

- name: Install SSH key for mirroring to GitHub
  copy: src=id_ed25519.vault dest={{ aur_user.home }}/.ssh/id_ed25519 owner={{ aur_user.name }} group={{ aur_user.name }} mode=0600

- name: Fetch host keys for github.com
  command: ssh-keyscan github.com
  args:
    creates: "{{ aur_user.home }}/.ssh/known_hosts"
  register: github_host_keys

- name: Write github.com host keys to the aur user's known_hosts
  lineinfile: name={{ aur_user.home }}/.ssh/known_hosts create=yes line={{ item }} owner={{ aur_user.name }} group={{ aur_user.name }} mode=0644
  loop: "{{ github_host_keys.stdout_lines }}"
  when: github_host_keys.changed

- name: Create directory
  file: path={{ aurweb_dir }} state=directory owner={{ aurweb_user }} group=http mode=0775

- name: Receive valid signing keys
  command: /usr/bin/gpg --keyserver keys.openpgp.org --recv {{ item }}
  loop: '{{ aurweb_pgp_keys }}'
  become: true
  become_user: "{{ aurweb_user }}"
  register: gpg
  changed_when: "gpg.rc == 0"

- name: Aurweb git repo check
  git: >
    repo={{ aurweb_repository }}
    dest="{{ aurweb_dir }}"
    version={{ aurweb_version }}
    verify_commit: true
    gpg_whitelist: '{{ aurweb_pgp_keys }}'
  become: true
  become_user: "{{ aurweb_user }}"
  register: release
  check_mode: true

- name: Install AUR systemd service and timers
  template: src={{ item }}.j2 dest=/etc/systemd/system/{{ item }} owner=root group=root mode=0644
  with_items:
    - aurweb-git.service
    - aurweb-git.timer
    - aurweb-aurblup.service
    - aurweb-aurblup.timer
    - aurweb-mkpkglists.service
    - aurweb-mkpkglists.timer
    - aurweb-pkgmaint.service
    - aurweb-pkgmaint.timer
    - aurweb-popupdate.service
    - aurweb-popupdate.timer
    - aurweb-tuvotereminder.service
    - aurweb-tuvotereminder.timer
    - aurweb-usermaint.service
    - aurweb-usermaint.timer
    - aurweb.service
    - aurweb-github-mirror.service
    - aurweb-github-mirror.timer
  when: release.changed

- name: Stop AUR systemd services and timers
  service: name={{ item }} enabled=yes state=stopped
  with_items:
    - aurweb-git.timer
    - aurweb-aurblup.timer
    - aurweb-mkpkglists.timer
    - aurweb-pkgmaint.timer
    - aurweb-popupdate.timer
    - aurweb-tuvotereminder.timer
    - aurweb-usermaint.timer
    - aurweb.service
    - aurweb-github-mirror.timer
  when: release.changed

- name: Clone aurweb repo
  git: >
    repo={{ aurweb_repository }}
    dest="{{ aurweb_dir }}"
    version={{ aurweb_version }}
    verify_commit: true
    gpg_whitelist: '{{ aurweb_pgp_keys }}'
  become: true
  become_user: "{{ aurweb_user }}"
  when: release.changed

- name: Create necessary directories
  file: path={{ aurweb_dir }}/{{ item }} state=directory owner={{ aurweb_user }} group={{ aurweb_user }} mode=0755
  with_items:
    - 'aurblup'
    - 'sessions'
    - 'uploads'

- name: Create aurweb conf dir
  file: path={{ aurweb_conf_dir }} state=directory owner=root group=root mode=0755

- name: Copy aurweb configuration file
  copy: src={{ aurweb_dir }}/conf/config.defaults dest={{ aurweb_conf_dir }}/config.defaults remote_src=yes owner=root group=root mode=0644

# Note: initdb needs the config
- name: Install custom aurweb configuration
  template: src=config.j2 dest={{ aurweb_conf_dir }}/config owner=root group=root mode=0644

- name: Create aur db
  mysql_db: name="{{ aurweb_db }}" login_host="{{ aurweb_db_host }}" login_password="{{ vault_mariadb_users.root }}" encoding=utf8
  register: db_created
  no_log: true

- name: Create aur db user
  mysql_user: name={{ aurweb_db_user }} password={{ vault_aurweb_db_password }}
              login_host="{{ aurweb_db_host }}" login_password="{{ vault_mariadb_users.root }}"
              priv="{{ aurweb_db }}.*:ALL"
  no_log: true

- name: Initialize the database
  command: poetry run python -m aurweb.initdb
  args:
    chdir: "{{ aurweb_dir }}"
  become: true
  become_user: "{{ aurweb_user }}"
  when: db_created.changed

- name: Run migrations
  command: poetry run alembic upgrade head
  args:
    chdir: "{{ aurweb_dir }}"
  environment:
    PYTHONPATH: .
  become: true
  become_user: "{{ aurweb_user }}"
  when: release.changed or db_created.changed

- name: Check python module availability  # noqa no-changed-when
  command: poetry run python3 -c 'import aurweb'
  args:
    chdir: "{{ aurweb_dir }}"
  become: true
  become_user: "{{ aurweb_user }}"
  ignore_errors: true
  register: aurweb_installed

- name: Install python module
  command: poetry install
  args:
    chdir: "{{ aurweb_dir }}"
  environment:
    POETRY_VIRTUALENVS_IN_PROJECT: "true"
    # https://github.com/python-poetry/poetry/issues/1917
    PYTHON_KEYRING_BACKEND: "keyring.backends.null.Keyring"
  become: true
  become_user: "{{ aurweb_user }}"
  when: release.changed or aurweb_installed.rc != 0

- name: Install custom aurweb-git-auth wrapper script
  template: src=aurweb-git-auth.sh.j2 dest=/usr/local/bin/aurweb-git-auth.sh owner=root group=root mode=0755
  when: release.changed

- name: Install custom aurweb-git-serve wrapper script
  template: src=aurweb-git-serve.sh.j2 dest=/usr/local/bin/aurweb-git-serve.sh owner=root group=root mode=0755
  when: release.changed

- name: Install custom aurweb-git-update wrapper script
  template: src=aurweb-git-update.sh.j2 dest=/usr/local/bin/aurweb-git-update.sh owner=root group=root mode=0755
  when: release.changed

- name: Link custom aurweb-git-update wrapper to hooks/update
  file:
    src: /usr/local/bin/aurweb-git-update.sh
    dest: "{{ aurweb_dir }}/aur.git/hooks/update"
    state: link
  when: release.changed

- name: Generate HTML documentation
  make:
    chdir: "{{ aurweb_dir }}/doc"
  become: true
  become_user: "{{ aurweb_user }}"

- name: Generate Translations
  make:
    chdir: "{{ aurweb_dir }}/po"
    target: "install"
  become: true
  become_user: "{{ aurweb_user }}"

- name: Create ssl cert
  include_role:
    name: certificate
  vars:
    domains: ["{{ aurweb_domain }}"]

- name: Set up nginx
  template: src=nginx.d.conf.j2 dest={{ aurweb_nginx_conf }} owner=root group=root mode=644
  notify: Reload nginx
  tags: ['nginx']

- name: Make nginx log dir
  file: path=/var/log/nginx/{{ aurweb_domain }} state=directory owner=root group=root mode=0755

- name: Install cgit configuration
  template: src=cgitrc.j2 dest="{{ aurweb_conf_dir }}/cgitrc" owner=root group=root mode=0644

- name: Configure cgit uwsgi service
  template: src=cgit.ini.j2 dest=/etc/uwsgi/vassals/cgit.ini owner={{ aurweb_user }} group=http mode=0644

- name: Deploy new cgit release
  become: true
  become_user: "{{ aurweb_user }}"
  file: path=/etc/uwsgi/vassals/cgit.ini state=touch owner=root group=root mode=0644
  when: cgit.changed

- name: Configure smartgit uwsgi service
  template: src=smartgit.ini.j2 dest=/etc/uwsgi/vassals/smartgit.ini owner={{ aurweb_user }} group=http mode=0644

- name: Deploy new smartgit release
  become: true
  become_user: "{{ aurweb_user }}"
  file:
    path: /etc/uwsgi/vassals/smartgit.ini
    state: touch
    owner: "{{ aurweb_user }}"
    group: http
    mode: 0644
  when: git.changed

- name: Create git repo dir
  file: path={{ aurweb_git_dir }} state=directory owner={{ aurweb_user }} group=http mode=0775

- name: Init git directory  # noqa command-instead-of-module
  command: git init --bare {{ aurweb_git_dir }}
  args:
    creates: "{{ aurweb_git_dir }}/HEAD"
  become: true
  become_user: "{{ aurweb_user }}"

- name: Save hideRefs setting on var  # noqa command-instead-of-module no-changed-when
  command: git config --local --get-all transfer.hideRefs
  register: git_config
  args:
    chdir: "{{ aurweb_git_dir }}"
  failed_when: git_config.rc == 2  # FIXME: does not work.

- name: Configure git tranfser.hideRefs  # noqa command-instead-of-module
  command: git config --local transfer.hideRefs '^refs/'
  args:
    chdir: "{{ aurweb_git_dir }}"
  become: true
  become_user: "{{ aurweb_user }}"
  when: git_config.stdout.find('^refs/') == -1

- name: Configure git transfer.hideRefs second  # noqa command-instead-of-module
  command: git config --local --add transfer.hideRefs '!refs/'
  args:
    chdir: "{{ aurweb_git_dir }}"
  become: true
  become_user: "{{ aurweb_user }}"
  when: git_config.stdout.find('!refs/') == -1

- name: Configure git transfer.hideRefs third  # noqa command-instead-of-module
  command: git config --local --add transfer.hideRefs '!HEAD'
  args:
    chdir: "{{ aurweb_git_dir }}"
  become: true
  become_user: "{{ aurweb_user }}"
  when: git_config.stdout.find('!HEAD') == -1

- name: Configure sshd
  template: src=aurweb_config.j2 dest={{ sshd_includes_dir }}/aurweb_config owner=root group=root mode=0600 validate='/usr/sbin/sshd -t -f %s'
  notify:
    - Restart sshd

- name: Start and enable AUR systemd services and timers
  service: name={{ item }} enabled=yes state=started daemon_reload=yes
  with_items:
    - aurweb-git.timer
    - aurweb-aurblup.timer
    - aurweb-mkpkglists.timer
    - aurweb-pkgmaint.timer
    - aurweb-popupdate.timer
    - aurweb-tuvotereminder.timer
    - aurweb-usermaint.timer
    - aurweb.service
    - aurweb-github-mirror.timer
  when: release.changed
